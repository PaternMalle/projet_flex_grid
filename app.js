//Boutons CONTENEUR
$(".fd").click(function() {
	$("#flexdirection").css("flex-direction", $(this).data("type"));
});

$(".jc").click(function() {
	$("#justifycontent").css("justify-content", $(this).data("type"));
});

$(".ai").click(function() {
	$("#alignitems").css("align-items", $(this).data("type"));
});

$(".ac").click(function() {
	$("#aligncontent").css("align-content", $(this).data("type"));
});

$(".fw").click(function() {
	$("#flexwrap").css("flex-wrap", $(this).data("type"));
});


//Boutons CONTENU align-self
$(".as").click(function() {
	$("#asitem03").css("align-self", $(this).data("type"));
});

//Boutons flex-grow
$("#cfg01").click(function() {
	$("#fgitem01").css("flex-grow", $(this).val());
});
$("#cfg02").click(function() {
	$("#fgitem02").css("flex-grow", $(this).val());
});
$("#cfg03").click(function() {
	$("#fgitem03").css("flex-grow", $(this).val());
});
$("#cfg04").click(function() {
	$("#fgitem04").css("flex-grow", $(this).val());
});
$("#cfg05").click(function() {
	$("#fgitem05").css("flex-grow", $(this).val());
});

//Boutons flex-shrink
$("#cfs01").click(function() {
	$("#fsitem01").css("flex-shrink", $(this).val());
});
$("#cfs02").click(function() {
	$("#fsitem02").css("flex-shrink", $(this).val());
});
$("#cfs03").click(function() {
	$("#fsitem03").css("flex-shrink", $(this).val());
});
$("#cfs04").click(function() {
	$("#fsitem04").css("flex-shrink", $(this).val());
});
$("#cfs05").click(function() {
	$("#fsitem05").css("flex-shrink", $(this).val());
});

//Boutons order
$("#co01").click(function() {
	$("#oitem01").css("order", $(this).val());
});
$("#co02").click(function() {
	$("#oitem02").css("order", $(this).val());
});
$("#co03").click(function() {
	$("#oitem03").css("order", $(this).val());
});
$("#co04").click(function() {
	$("#oitem04").css("order", $(this).val());
});
$("#co05").click(function() {
	$("#oitem05").css("order", $(this).val());
});

/*Grid*/

/*Display*/

$(".disp").click(function() {
	$("#display").css("display", $(this).data("type"));
});

/*Grid-template-columns*/

$(".ro").click(function() {
	$("#rows").css("grid-template-rows", $(this).data("type"));
});

/*Grid-template-rows*/

$(".col").click(function() {
	$("#columns").css("grid-template-columns", $(this).data("type"));
});

/*Grid-template-areas*/

$(".ar").click(function() {
	$("#areas").css("grid-template-areas", $(this).data("type"));
});

/*Gaps*/

$(".gar").click(function() {
	$("#gaps").css("row-gap", $(this).data("type"));
});

$(".gac").click(function() {
	$("#gaps").css("column-gap", $(this).data("type"));
});

$(".ga").click(function() {
	$("#gaps").css("gap", $(this).data("type"));
});

/*Grid-row*/

$(".grs").click(function() {
	$("#item1r").css("grid-row-start", $(this).data("type"));
});

$(".gre").click(function() {
	$("#item1r").css("grid-row-end", $(this).data("type"));
});

$(".grow").click(function() {
	$("#item1r").css("grid-row", $(this).data("type"));
});

/*Grid-column*/

$(".gcs").click(function() {
	$("#item1c").css("grid-column-start", $(this).data("type"));
});

$(".gce").click(function() {
	$("#item1c").css("grid-column-end", $(this).data("type"));
});

$(".gcol").click(function() {
	$("#item1c").css("grid-column", $(this).data("type"));
});

/*Grid-area*/

$(".gare").click(function() {
	$("#item1ar").css("grid-area", $(this).data("type"));
});

